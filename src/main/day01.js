import { arrayOfInts } from './days'

export const part1 = input => arrayOfInts(input).reduce((a, b) => a + b)

export const part2 = input => {
	const diff = arrayOfInts(input)
	const frequencies = new Set()
	let frequency = 0
	let i = 0

	while (!frequencies.has(frequency)) {
		frequencies.add(frequency)
		frequency += diff[i % diff.length]
		i++
	}

	return frequency
}
