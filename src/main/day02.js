import { arrayOfLines, alphabet } from './days'

export const part1 = input => {
	const codes = arrayOfLines(input)

	let nombreDeCodesAvdecLettreExactementEnDouble = 0
	let nombreDeCodesAvdecLettreExactementEnTriple = 0

	for (const code of codes) {
		const nombre = []

		for (let j = 0; j < alphabet.length; j++) {
			nombre[j] = 0
		}

		for (let i = 0; i < code.length; i++) {
			for (let j = 0; j < alphabet.length; j++) {
				if (code[i] === alphabet[j]) {
					nombre[j]++
				}
			}
		}

		let leCodeContientUneLettreExactementEnDouble = false
		let leCodeContientUneLettreExactementEnTriple = false
		for (let j = 0; j < alphabet.length; j++) {
			if (nombre[j] === 2) {
				leCodeContientUneLettreExactementEnDouble = true
			}
			if (nombre[j] === 3) {
				leCodeContientUneLettreExactementEnTriple = true
			}
		}

		if (leCodeContientUneLettreExactementEnDouble) {
			nombreDeCodesAvdecLettreExactementEnDouble++
		}
		if (leCodeContientUneLettreExactementEnTriple) {
			nombreDeCodesAvdecLettreExactementEnTriple++
		}
	}

	return nombreDeCodesAvdecLettreExactementEnDouble * nombreDeCodesAvdecLettreExactementEnTriple
}

export const part2 = input => {
	const codes = arrayOfLines(input)
	let retour = ''

	for (let i = 0; i < codes.length - 1; i++) {
		for (let j = i + 1; j < codes.length; j++) {
			for (let k = 0; k < codes[i].length; k++) {
				if (codes[i].substr(0, k) + codes[i].substr(k + 1) === codes[j].substr(0, k) + codes[j].substr(k + 1)) {
					retour = codes[i].substr(0, k) + codes[i].substr(k + 1)
				}
			}
		}
	}

	return retour
}
