import { arrayOfLines } from './days'

const regex = /#([0-9]+)\s@\s([0-9]+),([0-9]+):\s([0-9]+)x([0-9]+)$/
// const regex = /#(.*) @ (.*),(.*): (.*)x(.*)/
const dimension = 1000
let id = 0
let abcisse = 0
let ordonnee = 0
let largeur = 0
let hauteur = 0
let resultat = []
let retour = 0

// Création de la grille
const grille = []
for (let i = 0; i < dimension; i++) {
	grille[i] = []
	for (let j = 0; j < dimension; j++) {
		grille[i][j] = 0
	}
}

// eslint-disable-next-line import/prefer-default-export
export const part1 = input => {
	const lignes = arrayOfLines(input)

	// Remplissage de la grille
	for (const ligne of lignes) {
		if (regex.test(ligne)) {
			resultat = ligne.split(regex)
			// id = resultat[1]
			abcisse = resultat[2]
			ordonnee = resultat[3]
			largeur = resultat[4]
			hauteur = resultat[5]
			for (let i = 0; i < largeur; i++) {
				for (let j = 0; j < hauteur; j++) {
					grille[parseInt(i) + parseInt(abcisse)][parseInt(j) + parseInt(ordonnee)]++
				}
			}
		}
	}

	// Comptage des valeurs strictement supérieures à 1 dans la grille
	let retour = 0
	for (let i = 0; i < dimension; i++) {
		for (let j = 0; j < dimension; j++) {
			if (grille[i][j] > 1) {
				retour++
			}
		}
	}

	return retour
}

// eslint-disable-next-line import/prefer-default-export
export const part2 = input => {
	const lignes = arrayOfLines(input)

	// Remplissage de la grille
	for (const ligne of lignes) {
		if (regex.test(ligne)) {
			resultat = ligne.split(regex)
			abcisse = resultat[2]
			ordonnee = resultat[3]
			largeur = resultat[4]
			hauteur = resultat[5]
			for (let i = 0; i < largeur; i++) {
				for (let j = 0; j < hauteur; j++) {
					grille[parseInt(i) + parseInt(abcisse)][parseInt(j) + parseInt(ordonnee)]++
				}
			}
		}
	}

	// On reparcourt toutes les lignes et on cherche celle qui n'a que des valeurs 1.
	// C'est pas top, il y a sûrement mieux à faire.
	for (const ligne of lignes) {
		if (regex.test(ligne)) {
			resultat = ligne.split(regex)
			id = resultat[1]
			abcisse = resultat[2]
			ordonnee = resultat[3]
			largeur = resultat[4]
			hauteur = resultat[5]
			let cEstLaBonneLigne = true
			for (let i = 0; i < largeur; i++) {
				for (let j = 0; j < hauteur; j++) {
					if (grille[parseInt(i) + parseInt(abcisse)][parseInt(j) + parseInt(ordonnee)] > 1) {
						cEstLaBonneLigne = false
					}
				}
			}

			if (cEstLaBonneLigne) {
				retour = id
			}
		}
	}

	return parseInt(retour)
}
