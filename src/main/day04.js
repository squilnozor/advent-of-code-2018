/* eslint-disable no-console */
import { arrayOfLines } from './days'

const regexLine = /\[(.*)-(.*)-(.*) (.*):(.*)\] (.*)/
const regexAction = /(.*) #(.*) (.*) (.*)/
let resultLine = []
let resultAction = []
let year = ''
let month = ''
let day = ''
let hour = ''
let minute = ''
let action = ''
const events = []
const guards = []
let indexLine = 0
let firstMinuteAsleep = 0

class Guard {
	constructor(id) {
		this.id = id
		this.asleep = []
		for (let i = 0; i < 60; i++) {
			this.asleep[i] = 0
		}
	}

	sleeps(minuteStart, minuteEnd) {
		for (let i = 0; i < minuteEnd - minuteStart; i++) {
			this.asleep[parseInt(i) + parseInt(minuteStart)]++
		}
	}

	totalMinutesAsleep() {
		return this.asleep.reduce((a, b) => a + b)
	}

	minutesMostOftenAsleep() {
		let minute = 0
		let max = 0
		for (let i = 0; i < 60; i++) {
			if (parseInt(this.asleep[i]) > max) {
				minute = i
				max = parseInt(this.asleep[i])
			}
		}
		return minute
	}
}

// Création de la team des gardes
for (let i = 0; i < 10000; i++) {
	guards[i] = new Guard(i)
}

class Event {
	constructor(id, date, type, idGuard) {
		// L'identifiant d'un event est le numéro de la ligne qui l'a créé.
		this.id = id
		this.date = date
		this.type = type
		this.idGuard = idGuard
	}
}

// eslint-disable-next-line import/prefer-default-export
export const part1 = input => {
	const lines = arrayOfLines(input)

	// Création des événements
	for (const line of lines) {
		if (regexLine.test(line)) {
			resultLine = line.split(regexLine)
			year = resultLine[1]
			month = resultLine[2] - 1
			day = resultLine[3]
			hour = resultLine[4]
			minute = resultLine[5]
			action = resultLine[6]

			if (action === 'falls asleep') {
				events[indexLine] = new Event(parseInt(indexLine), new Date(Date.UTC(year, month, day, hour, minute)), 'fallsAsleep')
			} else if (action === 'wakes up') {
				events[indexLine] = new Event(parseInt(indexLine), new Date(Date.UTC(year, month, day, hour, minute)), 'wakesUp')
			} else if (regexAction.test(action)) {
				resultAction = action.split(regexAction)
				events[indexLine] = new Event(parseInt(indexLine), new Date(Date.UTC(year, month, day, hour, minute)), 'beginsShift', resultAction[2])
			}
		}
		indexLine++
	}

	// On va trier les événements par date.
	events.sort((a, b) => a.date - b.date)

	// On a besoin d'un garde. On prend n'importe lequel, de toute façon on va le changer tout de suite.
	let guardOnDuty = guards[0]

	// On va exploiter les events triés.
	for (let i = 0; i < events.length; i++) {
		// attention ! i n'est pas l'id de l'event (ni du garde ofc).

		if (events[i].type === 'beginsShift') {
			// Début de tour d'un garde. On identifie le garde.
			for (let j = 0; j < guards.length; j++) {
				if (parseInt(guards[j].id) === parseInt(events[i].idGuard)) {
					guardOnDuty = guards[j]
				}
			}
		} else if (events[i].type === 'fallsAsleep') {
			firstMinuteAsleep = events[i].date.getUTCMinutes()
		} else if (events[i].type === 'wakesUp') {
			guardOnDuty.sleeps(firstMinuteAsleep, events[i].date.getUTCMinutes())
		}
	}

	// On calcule quel garde dort le plus
	guardOnDuty = guards[0]
	// eslint-disable-next-line guard-for-in
	for (let i = 0; i < guards.length; i++) {
		if (parseInt(guards[i].totalMinutesAsleep()) > parseInt(guardOnDuty.totalMinutesAsleep())) {
			guardOnDuty = guards[i]
		}
	}

	return guardOnDuty.id * guardOnDuty.minutesMostOftenAsleep()
}

// eslint-disable-next-line import/prefer-default-export
export const part2 = input => {
	const lines = arrayOfLines(input)

	// Création des événements
	for (const line of lines) {
		if (regexLine.test(line)) {
			resultLine = line.split(regexLine)
			year = resultLine[1]
			month = resultLine[2] - 1
			day = resultLine[3]
			hour = resultLine[4]
			minute = resultLine[5]
			action = resultLine[6]

			if (action === 'falls asleep') {
				events[indexLine] = new Event(parseInt(indexLine), new Date(Date.UTC(year, month, day, hour, minute)), 'fallsAsleep')
			} else if (action === 'wakes up') {
				events[indexLine] = new Event(parseInt(indexLine), new Date(Date.UTC(year, month, day, hour, minute)), 'wakesUp')
			} else if (regexAction.test(action)) {
				resultAction = action.split(regexAction)
				events[indexLine] = new Event(parseInt(indexLine), new Date(Date.UTC(year, month, day, hour, minute)), 'beginsShift', resultAction[2])
			}
		}
		indexLine++
	}

	// On va trier les événements par date.
	events.sort((a, b) => a.date - b.date)

	// On a besoin d'un garde. On prend n'importe lequel, de toute façon on va le changer tout de suite.
	let guardOnDuty = guards[0]

	// On va exploiter les events triés.
	for (let i = 0; i < events.length; i++) {
		// attention ! i n'est pas l'id de l'event (ni du garde ofc).

		if (events[i].type === 'beginsShift') {
			// Début de tour d'un garde. On identifie le garde.
			for (let j = 0; j < guards.length; j++) {
				if (parseInt(guards[j].id) === parseInt(events[i].idGuard)) {
					guardOnDuty = guards[j]
				}
			}
		} else if (events[i].type === 'fallsAsleep') {
			firstMinuteAsleep = events[i].date.getUTCMinutes()
		} else if (events[i].type === 'wakesUp') {
			guardOnDuty.sleeps(firstMinuteAsleep, events[i].date.getUTCMinutes())
		}
	}

	// On calcule quelle minute est celle où un garde dort le plus
	let minuteMax = 0
	guardOnDuty = guards[0]
	let countMax = 0

	for (let i = 0; i < 60; i++) {
		for (let j = 0; j < guards.length; j++) {
			if (guards[j].asleep[i] > countMax) {
				countMax = guards[j].asleep[i]
				minuteMax = i
				guardOnDuty = guards[j]
			}
		}
	}

	return guardOnDuty.id * minuteMax
}
