/* eslint-disable no-console */
import { arrayOfLines, alphabet } from './days'

let polymer = ''
let indexPolymer = 0
let lengthReturn = 1000000

export const part1 = input => {
	const lines = arrayOfLines(input)
	console.log('nombre de lignes ', lines.length)

	// Lecture de la ligne (il n'y en a qu'une)
	for (const line of lines) {
		polymer = line
		while (indexPolymer < line.length) {
			if (polymer.charCodeAt(indexPolymer) - polymer.charCodeAt(indexPolymer + 1) === 32 || polymer.charCodeAt(indexPolymer + 1) - polymer.charCodeAt(indexPolymer) === 32) {
				polymer = polymer.slice(0, indexPolymer) + polymer.slice(indexPolymer + 2)
				indexPolymer -= 2
			}
			indexPolymer++
		}
	}

	return polymer.length
}

export const part2 = input => {
	const lines = arrayOfLines(input)
	console.log('nombre de lignes ', lines.length)

	// Lecture de la ligne (il n'y en a qu'une)
	for (const line of lines) {
		// eslint-disable-next-line guard-for-in
		for (let i = 0; i < alphabet.length; i++) {
			const lettre = alphabet[i]
			let polymerTest = line
				.split(lettre)
				.join('')
				.split(lettre.toUpperCase())
				.join('')

			let indexPolymerTest = 0

			while (indexPolymerTest < line.length) {
				if (
					polymerTest.charCodeAt(indexPolymerTest) - polymerTest.charCodeAt(indexPolymerTest + 1) === 32 ||
					polymerTest.charCodeAt(indexPolymerTest + 1) - polymerTest.charCodeAt(indexPolymerTest) === 32
				) {
					polymerTest = polymerTest.slice(0, indexPolymerTest) + polymerTest.slice(indexPolymerTest + 2)
					indexPolymerTest -= 2
				}
				indexPolymerTest++
			}

			if (polymerTest.length < lengthReturn) {
				lengthReturn = polymerTest.length
			}
		}
	}

	return lengthReturn
}
