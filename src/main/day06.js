/* eslint-disable no-console */
import { arrayOfLines } from './days'

const dimension = 400
const regex = /(.*), (.*)/
const spots = [] // Les points de références sur la grille (A, B, C, etc.)
const areaSize = [] // La surface de la zone d'influence de chaque spot
let distance = 0
let sizeOfSecureArea = 0

class Point {
	constructor(x, y) {
		this.x = x
		this.y = y
	}
}

/* Création de la grille */
const grille = []
for (let i = 0; i < dimension; i++) {
	grille[i] = []
	for (let j = 0; j < dimension; j++) {
		grille[i][j] = new Point(-1, -1)
	}
}

const manhattanDistance = (p1, p2) => {
	return Math.abs(p1.x - p2.x) + Math.abs(p1.y - p2.y)
}

export const part1 = input => {
	const lines = arrayOfLines(input)

	// Création des points de référence sur la grille
	for (let k = 0; k < lines.length; k++) {
		const coordinates = lines[k].split(regex)
		if (regex.test(lines[k])) {
			spots[k] = new Point(coordinates[1], coordinates[2])
			areaSize[k] = 0
		}
	}
	// On ajoute un dernier spot fictif (qui ne compte pas)
	spots[lines.length] = new Point(-1, -1)

	// On parcourt la grille
	for (let i = 0; i < dimension; i++) {
		for (let j = 0; j < dimension; j++) {
			const point = new Point(i, j)
			distance = dimension * 2 // C'est la distance max entre deux points de la grille

			// On parcourt tous les spots de référence, on calcule leur distance avec le point courant et on retient le plus proche.
			for (let k = 0; k < lines.length; k++) {
				if (manhattanDistance(point, spots[k]) === distance) {
					grille[i][j] = spots[lines.length]
				} else if (manhattanDistance(point, spots[k]) < distance) {
					distance = manhattanDistance(point, spots[k])
					grille[i][j] = spots[k]
				}
			}
		}
	}

	// On parcourt tous les spots et on compte les points qui leur sont affectés dans la grille.
	// Mais pour chaque point du bord de la grille affecté au spot, on soustrait 1000000000 afin de disqualifier le spot, car on considère que sa zone d'influence est infinie.
	for (let k = 0; k < lines.length; k++) {
		const spot = spots[k]
		for (let i = 0; i < dimension; i++) {
			for (let j = 0; j < dimension; j++) {
				if (grille[i][j] === spot) {
					areaSize[k]++
					if (i === 0 || i === dimension - 1 || j === 0 || j === dimension - 1) {
						areaSize[k] -= 1000000000
					}
				}
			}
		}
	}

	console.log('areaSize ', areaSize)

	return Math.max(...areaSize)
}

export const part2 = (input, secureDistance) => {
	const lines = arrayOfLines(input)

	// Création des points de référence sur la grille
	for (let k = 0; k < lines.length; k++) {
		const coordinates = lines[k].split(regex)
		if (regex.test(lines[k])) {
			spots[k] = new Point(coordinates[1], coordinates[2])
			areaSize[k] = 0
		}
	}

	for (let i = 0; i < dimension; i++) {
		for (let j = 0; j < dimension; j++) {
			let totalDistance = 0
			for (let k = 0; k < lines.length; k++) {
				totalDistance += manhattanDistance(new Point(i, j), spots[k])
			}
			if (totalDistance < parseInt(secureDistance)) {
				sizeOfSecureArea++
			}
		}
	}
	return sizeOfSecureArea
}
