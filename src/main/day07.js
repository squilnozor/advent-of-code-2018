/* eslint-disable no-cond-assign */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-loop-func */
/* eslint-disable guard-for-in */
/* eslint-disable no-console */
import { arrayOfLines } from './days'

const regex = /Step (.*) must be finished before step (.*) can begin./
let result = []
const steps = []
let stepSequenceReturned = ''

export const part1 = input => {
	const lines = arrayOfLines(input)
	for (let i = 0; i < lines.length; i++) {
		const line = lines[i]
		if (regex.test(line)) {
			result = line.split(regex)
			steps[i] = []
			steps[i][0] = result[1]
			steps[i][1] = result[2]
		}
	}

	// Solution qui ne marche pas ; c'est dommage, ça aurait pu être malin
	/* for (let i = 0; i < letters.length; i++) {
		for (let j = 0; j < letters.length; j++) {
			const initChar = letters[j]
			const idxInitChar = returnOrder.indexOf(initChar)
			for (let k = 0; k < steps.length; k++) {
				const finChar = letters[i]
				const idxFinChar = returnOrder.indexOf(finChar)
				const initChar = letters[j]
				const idxInitChar = returnOrder.indexOf(initChar)
				if (finChar === steps[k][1] && initChar === steps[k][0] && idxFinChar < idxInitChar) {
					console.log('finChar : ', finChar, ' index : ', idxFinChar, 'initChar : ', initChar, ' index : ', idxInitChar)
					returnOrder = returnOrder.substr(0, idxFinChar) + initChar + returnOrder.substr(idxFinChar, idxInitChar - idxFinChar) + returnOrder.substr(idxInitChar + 1)
					console.log('returnOrder ', returnOrder)
				}
			}
		}
	} */

	// À la place, on va faire bourrin avec plein de boucles pour remplir tous les spots du retour attendu en partant de la fin.
	// Avant tout, on met toutes les étapes au chaud dans un Set, qu'on videra au fur et à mesure.
	const remainingSteps = new Set()
	for (let i = 0; i < steps.length; i++) {
		remainingSteps.add(steps[i][0])
		remainingSteps.add(steps[i][1])
	}

	// Début de la boucle
	while (remainingSteps.size) {
		// On trouve les étapes qui ne dépendent d'aucune étape avant.
		const initialSteps = new Set()
		remainingSteps.forEach(step => {
			initialSteps.add(step)
		})
		for (let i = 0; i < steps.length; i++) {
			initialSteps.delete(steps[i][1])
		}

		const sortedInitialSteps = Array.from(initialSteps).sort()
		const initialStep = sortedInitialSteps[0]
		stepSequenceReturned += initialStep

		// On supprime toutes les lignes dont initialStep est la condition initiale
		let indexToRemove = 0
		while (indexToRemove < steps.length) {
			if (steps[indexToRemove][0] === initialStep) {
				steps.splice(indexToRemove, 1)
				indexToRemove -= 1
			}
			indexToRemove++
		}

		remainingSteps.delete(initialStep)
	}

	return stepSequenceReturned
}

export const part2 = (input, defaultDuration, numberOfWorkers) => {
	// Ça a l'air super compliqué ; je vais essayer avec des classes pour représenter les tâches à effectuer (et peut-être les travailleurs).

	let numberOfFreeWorkers = numberOfWorkers

	class Task {
		constructor(id, totalDuration, completion) {
			this.id = id
			this.totalDuration = totalDuration // temps nécessaire pour accomplir la têche (en secondes)
			this.completion = completion // nombre de secondes travaillées (sur un total de totalDuration). La tâche commence à être travaillée dès que completion >= 0
		}

		// Indique si la tâche reste à faire
		isRemaining() {
			let returned = false
			remainingTasks.forEach(task => {
				if (task.id === this.id) {
					returned = true
				}
			})
			return returned
		}

		// Renvoie l'ensemble des id des tâches qui doivent avoir été traitées avant
		stepsThatMustBeFinishedBefore() {
			const returned = new Set()
			for (let i = 0; i < steps.length; i++) {
				if (steps[i][1] === this.id) {
					returned.add(steps[i][0])
				}
			}
			return returned
		}

		// Indique si la tâche est dispo pour être lancée
		isAvailable() {
			let returned = this.isRemaining()
			this.stepsThatMustBeFinishedBefore().forEach(step => {
				if (stepToTask(step) && stepToTask(step).isRemaining()) {
					returned = false
				}
			})
			if (this.completion >= 0) {
				returned = false
			}
			return returned
		}

		progresses() {
			this.completion++
		}
	}

	// Renvoie une tâche à partir de son id
	const stepToTask = step => {
		let returned = null
		remainingTasks.forEach(task => {
			if (task.id === step) {
				returned = task
			}
		})
		return returned
	}

	// Renvoie parmi les tâches libres, l'identifiant de la première par ordre alphabétique. S'il n'y en a aucune, renvoie null.
	const nextAvailableTaskId = () => {
		let returned = null
		const availableSteps = []
		remainingTasks.forEach(task => {
			if (task.isAvailable()) {
				availableSteps.push(task.id)
			}
		})
		if (availableSteps.length) {
			returned = Array.from(availableSteps).sort()[0]
		}
		return returned
	}

	// Création des lignes
	const lines = arrayOfLines(input)
	for (let i = 0; i < lines.length; i++) {
		const line = lines[i]
		if (regex.test(line)) {
			result = line.split(regex)
			steps[i] = []
			steps[i][0] = result[1]
			steps[i][1] = result[2]
		}
	}

	// Création des tâches à réaliser
	const remainingSteps = new Set()
	for (let i = 0; i < steps.length; i++) {
		remainingSteps.add(steps[i][0])
		remainingSteps.add(steps[i][1])
	}
	const remainingTasks = new Set()
	remainingSteps.forEach(step => {
		remainingTasks.add(new Task(step, step.charCodeAt(0) + defaultDuration - 64, -1))
	})
	console.log('remaining tasks', remainingTasks)

	let timeElapsed = 0 // durée totale en secondes

	// Début de la boucle : on va boucler tant qu'il reste des tâches qui ne sont pas terminées.
	while (remainingTasks.size) {
		console.log('Début du tour', timeElapsed, '. Il y a', numberOfFreeWorkers, 'travailleurs disponibles.')

		// On fait avancer les tâches en cours
		remainingTasks.forEach(task => {
			if (task.completion >= 0 && task.completion < task.totalDuration) {
				task.progresses()
				// console.log('La tâche', task.id, 'progresse. Elle en est à', task.completion, 'secondes.')
			}
		})

		// Si une tâche est terminée, on la sort de la liste des tâches à faire
		remainingTasks.forEach(task => {
			if (task.completion === task.totalDuration) {
				// Je pense qu'il y aura un problème si deux tâches s'achèvent en même temps ; dans ce cas, il faudra faire un while () avec un index. On verra...
				remainingTasks.delete(task)
				// console.log('La tâche', task.id, 'a été retirée du reste à faire.')
				numberOfFreeWorkers++
				// console.log('Un travailleur a été libéré. Il reste', numberOfFreeWorkers, 'travailleur(s) disponible(s).')
			}
		})

		// S'il y a une tâche libre et un travailleur libre, on affecte ce travailleur à cette tâche
		// (S'il y a plusieurs tâches libres, on prend la première par ordre alphabétique).
		// S'il y a plusieurs travailleurs libres et plusieurs tâches disponibles, on peut affecter plusieurs travailleurs à plusieurs tâches.
		while (nextAvailableTaskId() != null && numberOfFreeWorkers > 0) {
			const task = stepToTask(nextAvailableTaskId())
			task.progresses()
			numberOfFreeWorkers--
			// console.log('Un travailleur a été affecté à la tâche', task.id, '. Il reste', numberOfFreeWorkers, 'travailleur(s) disponible(s).')
		}

		timeElapsed++
	}

	return timeElapsed - 1
}
