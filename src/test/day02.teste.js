import { part1, part2 } from '../main/day02'

describe('Day 02, part 1', () => {
	test('the checksum is 12', () => {
		expect(part1('src/test/resources/02-01.txt')).toBe(12)
	})
	test('the checksum is 12', () => {
		expect(part1('src/main/resources/02-01.txt')).toBe(5681)
	})
})

describe('Day 02, part 2', () => {
	test('characters are fgij', () => {
		expect(part2('src/test/resources/02-02.txt')).toBe('fgij')
	})
	test('the answer to part 2 is 78724', () => {
		expect(part2('src/main/resources/02-02.txt')).toBe('uqyoeizfvmbistpkgnocjtwld')
	})
})
