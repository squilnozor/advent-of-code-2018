import { part1, part2 } from '../main/day03'

describe('Day 03, part 1', () => {
	test('the number of shared squares is 4', () => {
		expect(part1('src/test/resources/03-01.txt')).toBe(4)
	})
	test('the number of shared squares is 4', () => {
		expect(part1('src/main/resources/03-01.txt')).toBe(120408)
	})
})

describe('Day 03, part 2', () => {
	test('the looked for identifier is 3', () => {
		expect(part2('src/test/resources/03-02.txt')).toBe(3)
	})
	test('the answer to part 2 is 1276', () => {
		expect(part2('src/main/resources/03-02.txt')).toBe(1276)
	})
})
