import { part1, part2 } from '../main/day04'

describe('Day 04, part 1', () => {
	test('the answer to part 1 (test) is 240', () => {
		expect(part1('src/test/resources/04-01.txt')).toBe(240)
	})
	test('the answer to part 1 (real case) is 19874', () => {
		expect(part1('src/main/resources/04-01.txt')).toBe(19874)
	})
})

describe('Day 04, part 2', () => {
	test('the answer to part 2 (test) is 4455', () => {
		expect(part2('src/test/resources/04-02.txt')).toBe(4455)
	})
	test('the answer to part 2 (real case) is 22687', () => {
		expect(part2('src/main/resources/04-02.txt')).toBe(22687)
	})
})
