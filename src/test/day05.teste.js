import { part1, part2 } from '../main/day05'

describe('Day 05, part 1', () => {
	test('the resulting polymers length is 10', () => {
		expect(part1('src/test/resources/05-01.txt')).toBe(10)
	})
	test('the resulting polymers length is 11476', () => {
		expect(part1('src/main/resources/05-01.txt')).toBe(11476)
	})
})

describe('Day 05, part 2', () => {
	test('the resulting polymers length is 4', () => {
		expect(part2('src/test/resources/05-02.txt')).toBe(4)
	})
	test('the resulting polymers length is 5446', () => {
		expect(part2('src/main/resources/05-02.txt')).toBe(5446)
	})
})
