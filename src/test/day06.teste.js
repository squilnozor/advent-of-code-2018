import { part1, part2 } from '../main/day06'

describe('Day 06, part 1', () => {
	test('the largest area is 17', () => {
		expect(part1('src/test/resources/06-01.txt')).toBe(17)
	})
	test('the largest area is 4143', () => {
		expect(part1('src/main/resources/06-01.txt')).toBe(4143)
	})
})

describe('Day 06, part 2', () => {
	test('the size of the secure region is 16', () => {
		expect(part2('src/test/resources/06-02.txt', 32)).toBe(16)
	})
	test('the size of the secure region is 35039', () => {
		expect(part2('src/test/resources/06-02.txt', 10000)).toBe(35039)
	})
})
