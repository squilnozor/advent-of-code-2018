import { part1, part2 } from '../main/day07'

describe('Day 07, part 1', () => {
	/* test('the right order is CABDFE', () => {
		expect(part1('src/test/resources/07-01.txt')).toBe('CABDFE')
	}) */
	/* test('the right order is BGKDMJCNEQRSTUZWHYLPAFIVXO', () => {
		expect(part1('src/main/resources/07-01.txt')).toBe('BGKDMJCNEQRSTUZWHYLPAFIVXO')
	}) */
})

describe('Day 07, part 2', () => {
	// On entre en paramètres la durée de base d'une action et le nombre de travailleurs
	/* test('the total duration is 15', () => {
		expect(part2('src/test/resources/07-02.txt', 0, 2)).toBe(15)
	}) */
	test('the total duration is 941', () => {
		expect(part2('src/main/resources/07-02.txt', 60, 6)).toBe(941)
	})
})
